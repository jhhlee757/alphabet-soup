import sys

# each node depicts a branch point in the prefix tree
class Node:
  def __init__(self):
    self.d = {}
    self.end = False

# a prefix tree that will store all the words we need to search
class Trie:
  def __init__(self):
    self.root = Node()

  # inserts the word into the prefix tree
  def insert(self, word):
    curr = self.root
    for c in word:
      if c not in curr.d:
        curr.d[c] = Node()
      curr = curr.d[c]
    curr.end = True
  
  # removes the word from the prefix tree
  def remove(self, word):
    curr = self.root
    parentAndChar = []
    for c in word:
      parentAndChar.append([curr, c])
      curr = curr.d[c]
    for parent, char in reversed(parentAndChar):
      del parent.d[char]
      if len(parent.d.keys()) != 0:
        break

def main():
  # take a filepath as input
  filepath = sys.argv[1]

  with open(filepath, "r") as file:
    lines = file.readlines()

    # get the number of rows and cols of the grid
    dimensions = lines[0].strip().split("x")
    ROWS = int(dimensions[0])
    COLS = int(dimensions[1])

    # make a 2d array representation of the grid
    grid = [["" for i in range(COLS)] for j in range(ROWS)]
    for i in range(0, ROWS):
      row = lines[i+1].strip()
      col = row.split(" ")
      for j, letter in enumerate(col):
        grid[i][j] = letter
    
    # get the words we need to search for (and remove any spaces in words)
    words = []
    for i in range(ROWS+1, len(lines)):
      words.append(lines[i].strip().replace(" ", ""))

  # insert all the words into a prefix tree
  trie = Trie()
  for word in words:
    trie.insert(word)

  def findWords(r, c, node, word, direction):
    if r < 0 or c < 0 or r >= ROWS or c >= COLS or (r,c) in visited or grid[r][c] not in node.d:
      return
    
    visited.add((r,c))
    word += grid[r][c]

    nextNode = node.d[grid[r][c]]
    if nextNode.end:
      foundWords.append((word, str(r) + ":" + str(c)))
      trie.remove(word)

    if not direction:
      for row, col in [[r+1, c], [r-1, c], [r, c+1], [r, c-1], [r-1, c-1], [r-1, c+1], [r+1, c-1], [r+1, c+1]]:
        findWords(row, col, nextNode, word, [row-r, col-c])
    else:
        findWords(r+direction[0], c+direction[1], nextNode, word, direction)
  
  # for each possible (r,c) set, run the helper function and save the found words in a dictionary 
  # with the word as the key and the output format as the value
  foundWordsDict = {}
  for r in range(ROWS):
    for c in range(COLS):
      visited = set()
      foundWords = []
      findWords(r, c, trie.root, "", None)
      
      for word, wordEnding in foundWords:
        s = word + " " + str(r) + ":" + str(c) + " " + wordEnding
        foundWordsDict[word] = s

  for word in words:
    if word in foundWordsDict:
      print(foundWordsDict[word])

if __name__ == "__main__":
  main()

# command to execute the code
# python3 alphabet-soup.py alphabet-soup.txt